import Connection from './Connection'

export default class ConnectionManager extends Connection {

  constructor (config) {
    super(config)
  }

  autoloadSchema () {
    const config = this.config.schema,
      autolad = config.autoload || (!config.autoload && config.tables.length)
    
    return Promise.try(() => {
      if (!autoload) {
        return
      }
      return this.listTables().then(res => {
        let tables = res
        if (!config.autoload && config.tables.length) {
          tables = res.map(table => {
            return config.tables.indexOf(table) > -1
          })
        }
        return Promise.map(tables, table => {
          return this.describeTable(table)
        })
      }).then(res => {
        this.tables = res
      })
    })
  }

  listTables () {
    return this.handler.listTables({}).promise().then(res => {
      return (!!res.TableNames) ? res.TableNames : []
    })
  }

  describeTable (table) {
    return this.handler.describeTable({Table: table}).promise().then(res => {
      if (!!res.Table && Object.keys(res.Table).length) {
        return res.Table
      }
      return (!!res.Table) ? res.Table : {}
    })
  }

  getConnection () {
    return this.autoloadSchema().then(() => {
      return super
    })
  }
}