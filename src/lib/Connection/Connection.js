import AWS from 'aws-sdk'
import Configuration from '../Configuration/Configuration'
import Driver from '../Driver/Driver'

export default class Connection {

  constructor (config) {

    this.config = config || new Configuration()

    this.driver = new Driver(this.config)

    AWS.config.update(this.config.getCredentials())

    this.handler = new AWS.DynamoDB()

    this.docClient = new AWS.DynamoDB.DocumentClient()

    this.tables = []
  }
}