import Type from '../Types/Type'
import Field from './Field'
import Index from './Index'
import FieldCollection from './FieldCollection'
import IndexCollection from './IndexCollection'

const Table = ((name, fields = []) => {
  const 
    fieldCollection = new FieldCollection(name, FieldCollection.TABLE),
    indexCollection = new IndexCollection(name)

  return class Table {
    
    constructor(name, fields = [], indexes = []) {

      this.name = name
      
      if (!(fields instanceof Array) || !(indexes instanceof Array)) {
        throw Exception.unexpectedConstructor(Array)
      }

      fields.forEach(field => {
        fieldCollection.addField(field)
      })

      indexes.forEach(index => {
        indexCollection.addField(index)
      })
    }

    set name (name) {
      this._name = name
      return this
    }

    get name () {
      return this._name
    }

    addField(fieldName, typeName) {
      const field = new Field(fieldName, Type.getType(typeName))
      fieldCollection.addField(field)
      return this
    }

    addKeySchema(indexName) {
      const index = new Index(indexName, Index.KEY_SCHEMA)
      indexCollection.addIndex(index)
      return this
    }

    addGlobalSecondaryIndex(indexName, fieldNames = []) {
      this._addSecondaryIndex(Index.GLOBAL_SECONDARY, indexName, fieldNames = [])
      return this
    }

    addLocalSecondaryIndex(indexName, fieldNames = []) {
      this._addSecondaryIndex(Index.LOCAL_SECONDARY, indexName, fieldNames = [])
      return this
    }

    _addSecondaryIndex(type, indexName, fieldNames = []) {
      const 
        fields = fieldCollection.getFields(fieldNames),
        index = new Index(indexName, type, fields)
      
      indexCollection.addIndex(index)
    }
  } 
})()

export default Table