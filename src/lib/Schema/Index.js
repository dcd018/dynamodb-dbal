import Field from './Field'
import FieldCollection from './FieldCollection'
import Exception from './Exception'
import DBALException from '../DBALException'

const Index = ((name, type, fields) {
  const fieldCollection = new FieldCollection(name, FieldCollection.INDEX)

  return class Index {

    constructor(name, type, fields = []) {
      
      this.name = name

      this.type = type

      if (!(fields instanceof Array)) {
        throw Exception.unexpectedConstructor(Array)
      }

      fields.forEach(field => {
        this.addField(field)
      })
    }

    static get KEY_SCHEMA() {return 'key_schema'}

    static get GLOBAL_SECONDARY() {return 'global_secondary'}

    static get LOCAL_SECONDARY() {return 'local_secondary'}

    static get types() {
      return [
        this.KEY_SCHEMA,
        this.GLOBAL_SECONDARY,
        this.LOCAL_SECONDARY
      ]
    }

    set name (name) {
      this._name = name
      return this
    }

    get name () {
      return this._name
    }

    set type (type) {
      if (this.types.indexOf(type) < 0) {
        throw DBALException.unknownIndexType(type)
      }
      this._type = type
      return this
    }

    get type () {
      return this._type
    }

    addField(field) {
      if (this.type == this.KEY_SCHEMA) {
        throw DBALException.unexpectedProjection()
      }
      fieldCollection.addField(field)
      return this
    }
  }
})()

export default Index