import FieldCollection from './FieldCollection'
import Exception from '../Exception'

export default class SchemaException {

  getException(message) {
    return new Exception(this.constructor.name, message)
  }

  invalidField() {
    return this.getException(`Invalid field specified.`)
  }

  fieldAlreadyExists(name, fieldName, type = FieldCollection.TABLE) {
    return this.getException(`The field ${fieldName} on ${type} ${name} already exists.`)
  }

  fieldDoesNotExist(name, fieldName, type = FieldCollection.TABLE) {
    return this.getException(`There is no field with name ${fieldName} on ${type} ${name}.`)
  }

  indexAlreadyExists(name, indexName) {
    return this.getException(`The index ${indexName} on table ${name} already exists.`)
  }

  indexDoesNotExist(name, indexName) {
    return this.getException(`There is no index with name ${indexName} on table ${name}.`)
  }
}