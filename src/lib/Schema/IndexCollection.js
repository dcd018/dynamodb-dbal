import Index from './Index'
import Exception from './Exception'
import SchemaException from './SchemaException'
import DBALException from '../DBALException'

class IndexCollection {

  constructor(name) {

    this.name = name

    this._indexes = []
  }

  set name (name) {
    this._name = name
    return this
  }

  get name () {
    return this._name
  }

  addIndex(index) {
    if (!(index instanceof Index)) {
      throw Exception.unexpectedConstructor(Index)
    }

    if (!!this._indexes[index.name]) {
      throw SchemaException.indexAlreadyExists(this._name, index.name)
    }
    this._indexes.push(index)
    return this
  }
}

export default IndexCollection