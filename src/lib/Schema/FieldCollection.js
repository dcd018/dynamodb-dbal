import Field from './Field'
import Exception from './Exception'
import SchemaException from './SchemaException'
import DBALException from '../DBALException'

class FieldCollection {

  constructor(name, type) {

    this.name = name

    this.type = type

    this._fields = {}
  }

  static get TABLE() {return 'table'}

  static get INDEX() {return 'index'}

  static get types() {
    return [
      this.TABLE,
      this.INDEX
    ]
  }

  set name (name) {
    this._name = name
    return this
  }

  get name () {
    return this._name
  }

  set type (type) {
    if (this.types.indexOf(type) < 0) {
      throw DBALException.unknownFieldCollectionType(type)
    }
    this._type = type
    return this
  }

  get type () {
    return this._type
  }

  addField(field) {
    if (!(field instanceof Field)) {
      throw Exception.unexpectedConstructor(Field)
    }

    if (!!this._fields[field.name]) {
      throw SchemaException.fieldAlreadyExists(this._name, field.name, this.type)
    }
    this._fields.push(field)
    return this
  }

  getFields(fieldNames = []) {
    if (!(fieldNames instanceof Array)) {
      throw Exception.unexpectedConstructor(Array)
    }
    if (!fieldNames.length) {
      return fieldNames
    }

    return fieldNames.map(fieldName => {
      if (!this._fields[fieldName]) {
        throw SchemaException.fieldDoesNotExist(this._name, fieldName)
      }
      return this._fields[fieldName]
    })
  }
}

export default FieldCollection