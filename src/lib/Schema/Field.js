import Type from '../Types/Type'
import Exception from '../Exception/Exception'

return class Field {

  constructor(name, type) {
    this.name = name

    this.type = type
  }

  set name (name) {
    this._name = name
    return this
  }

  get name () {
    return this._name
  }

  set type (t) {
    if (!(t instanceof Type)) {
      throw Exception.unexpectedConstructor(Type)
    }
    this._type = t
    return this
  }

  get type () {
    return this._type
  }
}