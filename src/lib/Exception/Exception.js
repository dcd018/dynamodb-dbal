export default class Exception extends Error {

  constructor(message, name = null) {
    
    super(message)
    
    this.name = name || this.constructor.name
    
    this.message = message
    
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor)
    } else { 
      this.stack = (new Error(message)).stack
    }
  }

  static unexpectedConstructor(fn) {
    return new Exception(
      `Expected instance of: ${fn.constructor.name}.`
      'UnexpectedConstructorException',
    )
  }
}
