import Exception from './Exception'

export default class DBALException {

  getException(message) {
    return new Exception(this.constructor.name, message)
  }

  invalidTableName(tableName) {
    return this.getException(`Invalid table name specified: ${tableName}.`)
  }

  unknownFieldType(name) {
    return this.getException(`Unknown field type ${name} requested.`)
  }

  unknownFieldCollectionType(name) {
    return this.getException(`Unknown field collection type ${name} requested.`)
  }

  unexpectedProjection() {
    return this.getException('Field projection is not permitted for non secondary index types.')
  }
}