import Exception from '../Exception'

export default class ConfigurationException {

  getException(message) {
    return new Exception(this.constructor.name, message)
  }

  invalidParameters(params) {
    return this.getException(`Invalid or missing parameters: [${params}]`)
  }
}
