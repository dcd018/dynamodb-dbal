import ConfigurationException from './ConfigurationException'

export default class Configuration {
  
  constructor (accessKeyId, secretAccessKey, region, apiVersion = '2012-08-10') {

    this.retryCodes = [
      'ItemCollectionSizeLimitExceededException',
      'ProvisionedThroughputExceededException',
      'LimitExceededException',
      'LimitExceededException',
      'ThrottlingException'
    ]

    this.retryLimit = -1

    this.schema = {
      autoload: true,
      tables: []
    }

    if (!!accessKeyId && typeof accessKeyId === 'object') {
      const params = accessKeyId, iam = params.credentials || {}

      this.credentials = {
        accessKeyId: iam.accessKeyId || process.env.ACCESS_KEY_ID,
        secretAccessKey: iam.secretAccessKey || process.env.SECRET_ACCESS_KEY
      }

      this.region = params.region || process.env.REGION

      this.apiVersions = {
        dynamodb: params.apiVersion || apiVersion
      }
    } 
    else {
      this.credentials = {
        accessKeyId: accessKeyId || process.env.ACCESS_KEY_ID, 
        secretAccessKey: secretAccessKey || process.env.SECRET_ACCESS_KEY
      }

      this.region = region || process.env.REGION

      this.apiVersions = {
        dynamodb: apiVersion
      }
    }

    const err = this.getInvalidParameters()
    if (err.length) {
      throw new ConfigurationException.invalidParameters(err.join(','))
    }
  }

  getInvalidParameters (params) {
    const that = params || this
    return Object.keys(that).map(key => {
      return (typeof that[key] === 'object') 
        ? this.getInvalidParameters(that[key])
        : (typeof that[key] !== 'function' && !that[key])
    })
  }

  getCredentials () {
    const {
      credentials, 
      region, 
      apiVersions
    } = this
    
    return {credentials, region, apiVersions}
  }
}