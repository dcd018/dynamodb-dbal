import Type from './Type'

export default class ListType {

  get name() {
    return Type.LIST
  }

  convert(value) {
    return value
  }
}