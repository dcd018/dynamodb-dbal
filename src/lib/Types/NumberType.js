import Type from './Type'

export default class NumberType {

  get name() {
    return Type.NUMBER
  }

  convert(value) {
    return value
  }
}