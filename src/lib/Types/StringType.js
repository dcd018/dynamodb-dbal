import Type from './Type'

export default class StringType {

  get name() {
    return Type.STRING
  }

  convert(value) {
    return value
  }
}