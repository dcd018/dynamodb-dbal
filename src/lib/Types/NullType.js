import Type from './Type'

export default class NullType {

  get name() {
    return Type.NULL
  }

  convert(value) {
    return null
  }
}