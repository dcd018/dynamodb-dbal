import Type from './Type'

export default class NumberSetType {

  get name() {
    return Type.NUMBER_SET
  }

  convert(value) {
    return value
  }
}