import BinarySetType from './BinarySetType'
import BinaryType from './BinaryType'
import BooleanType from './BooleanType'
import ListType from './ListType'
import MapType from './MapType'
import NullType from './NullType'
import NumberSetType from './NumberSetType'
import NumberType from './NumberType'
import StringSetType from './StringSetType'
import StringType from './StringType'

import DBALException from '../DBALException'

const Type = (() => {
  const typeObjects = []

  return class Type {

    static get BINARY_SET() {return 'binary_set'}

    static get BINARY() {return 'binary'}

    static get BOOLEAN() {return 'boolean'}

    static get LIST() {return 'list'}

    static get MAP() {return 'map'}

    static get NULL() {return 'null'}

    static get NUMBER_SET() {return 'number_set'}

    static get NUMBER() {return 'number'}

    static get STRING_SET() {return 'string_set'}

    static get STRING() {return 'string'}

    static get typesMap() {
      this.BINARY_SET: BinarySetType,
      this.BINARY: BinaryType,
      this.BOOLEAN: BooleanType,
      this.LIST: ListType,
      this.MAP: MapType,
      this.NULL: NullType,
      this.NUMBER_SET: NumberSetType,
      this.NUMBER: NumberType,
      this.STRING_SET: StringSetType,
      this.STRING: StringType
    }

    static getType(name) {
      if (!typeObjects[name]) {
        if (!this.typesMap[name]) {
          throw DBALException.unknownFieldType(name)
        }
        typeObjects[name] = new this.typesMap[name]()
      }
      return typeObjects[name]
    }
  }
})()

export default Type