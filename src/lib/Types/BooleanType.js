import Type from './Type'

export default class BooleanType {

  get name() {
    return Type.BOOLEAN
  }

  convert(value) {
    const f = ['no', 'n', 0]
    return (typeof value !== 'boolean') ? !(f.indexOf(value) > -1) : value
  }
}