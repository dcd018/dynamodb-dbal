import Type from './Type'

export default class StringSetType {

  get name() {
    return Type.STRING_SET
  }

  convert(value) {
    return value
  }
}