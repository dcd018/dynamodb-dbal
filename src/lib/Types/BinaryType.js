import Type from './Type'

export default class BinaryType {

  get name() {
    return Type.BINARY
  }

  convert(value) {
    if (value instanceof Buffer) {
      return value.toString('base64')
    }
    
    const regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/
    if (!regex.test(value)) {
      const buff = new Buffer(value)
      return buff.toString('base64')
    }
    return value
  }
}