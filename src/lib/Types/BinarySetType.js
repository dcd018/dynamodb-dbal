import Type from './Type'
import BinaryType from './BinaryType'

export default class BinarySetType extends BinaryType {

  get name() {
    return Type.BINARY_SET
  }

  convert(value) {
    value.forEach(item => {
      item = super.convert(item)
    })
    return value
  }
}