import Type from './Type'

export default class MapType {

  get name() {
    return Type.MAP
  }

  convert(value) {
    return value
  }
}