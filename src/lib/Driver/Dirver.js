import Configuration from '../Configuration/Configuration'
import ConfigurationException from '../Configuration/ConfigurationException'

export default class Driver {

  constructor (config) {
    this.config = config
    if (instanceof this.config !== Configuration) {
      throw new ConfigurationException('Invalid Configuration object detected')
    }
  }

  sizeOf (items) {
    const byteSizes = {
      boolean: elm => {
        return 8
      },
      string: elm => {
        return elm.length * 2 
      },
      number: elm => {
        return 4
      },
      object: elm => {
        let bytes = 0
        const objClass = Object.prototype.toString.call(obj).slice(8, -1)

        if (['Object', 'Array'].indexOf(objClass) > -1) {
          for (const key in elm) {
            if (!Object.hasOwnProperty(key)) continue
            bytes += this.sizeOf(elm[key])
          }
        } 
        else {
          bytes += this.sizeOf(elm.toString())
        }
        return bytes
      }
    }
    const bytes = byteSizes[typeof items](items)
    return (typeof bytes === 'number' && !isNaN(bytes)) ? bytes : 0
  }

  calculateThroughput (items = [], unitType = 'read', consistent = false) {
    const bytes = this.sizeOf(items)
    if (bytes > 1024) {
      let units = (bytes / 1024).toFixed(3)
      units = (unitType == 'read') ? (units / 4) * ((consistent) ? 2 : 1) : units
      return Math.ceil(units)
    }
    return 1
  }

  exec (promise) {
    let retries = 0
    return (function exec () {
      return promise().catch(err => {
        retries++
        const 
          {retryCodes, requestLimit} = this.config,
          retry = !!err.code && retryCodes.indexOf(err.code) > -1,
          limit = (!isNaN(requestLimit)) ? requestLimit : -1
        
        if (retry && (limit < 0 || (limit > 1 && retries < limit))) {
          return Promise.delay(Math.pow(retries, 2) * 50).then(res => {
            return exec(promise)
          })
        }
        throw err
      })
    })()
  }
}